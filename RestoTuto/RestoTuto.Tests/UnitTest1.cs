﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestoTuto.Models;

namespace RestoTuto.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            RestoList restos = new RestoList();
            List<Resto> ListTest = restos.ObtientTousLesRestaurants();

            ListTest = restos.CreerUnRestaurant(ListTest, "La bonne fourchette", "0123456789");

            Assert.IsNotNull(ListTest);
            Assert.AreEqual(6, ListTest.Count);
            Assert.AreEqual("La bonne fourchette", ListTest[5].Nom);
            Assert.AreEqual("0123456789", ListTest[5].Telephone);
        }

        [TestMethod]
        public void TestMethod2()
        {
            RestoList restos = new RestoList();
            List<Resto> ListTest = restos.ObtientTousLesRestaurants();

            ListTest = restos.CreerUnRestaurant(ListTest, "La bonne fourchette", "0123456789");
            restos.AjouterUnRestaurant("RestoSix", "0606060606");



            ListTest = restos.ChangerRestaurant(ListTest, 5, "La fourchette bonne", "9876543210");

            Assert.AreEqual("La fourchette bonne", ListTest[5].Nom);
            Assert.AreEqual("9876543210", ListTest[5].Telephone);

            restos.ModifierRestaurant(5, "RestoSept", "0707070707");

            Assert.AreEqual("RestoSept", restos.ObtientTousLesRestaurants()[5].Nom);
            Assert.AreEqual("0707070707", restos.ObtientTousLesRestaurants()[5].Telephone);
        }
    }
}
