﻿using RestoTuto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RestoTuto.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            RestoList r = new RestoList();
            return View(r);
        }

        public ActionResult CreateResto()
        {
            RestoList r = new RestoList();
            return PartialView("CreateResto", r);
        }

        [HttpPost]
        public ActionResult UpdateRestos(RestoList r)
        {
            Console.WriteLine("Check");
//            System.Diagnostics.Debug.WriteLine(r.Resto.Nom);
            return RedirectToAction("Index");
        }

        public ActionResult Details(int restoId)
        {
            RestoList r = new RestoList();
            Resto resto = r.GetRestoById(restoId);
            return PartialView("Details", resto);
        }
    }
}