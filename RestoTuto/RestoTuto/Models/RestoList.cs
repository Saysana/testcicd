﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RestoTuto.Models
{
    public class RestoList
    {
        private List<Resto> Restos;
        public List<SelectListItem> RestosNames;
        public Resto Resto;
        public SelectListItem RestoName;

        public RestoList()
        {
            RestosNames = new List<SelectListItem>();
            Restos = new List<Resto>();
            RestoName = new SelectListItem();

            Restos.Add(new Resto { Id = 0, Nom = "RestoUn", Telephone = "0101010101" });
            Restos.Add(new Resto { Id = 1, Nom = "RestoDeux", Telephone = "0202020202" });
            Restos.Add(new Resto { Id = 2, Nom = "RestoTrois", Telephone = "0303030303" });
            Restos.Add(new Resto { Id = 3, Nom = "RestoQuatre", Telephone = "0404040404" });
            Restos.Add(new Resto { Id = 4, Nom = "RestoCinq", Telephone = "0505050505" });
            
            foreach (var elem in Restos)
            {
                RestosNames.Add(new SelectListItem { Text = elem.Nom, Value = RestosNames.Count.ToString() });
            }
        }

        public List<Resto> ObtientTousLesRestaurants()
        {
            return Restos;
        }

        public List<Resto> CreerUnRestaurant(List<Resto> restos, string nom, string telephone)
        {
            restos.Add(new Resto { Id = restos.Count, Nom = nom, Telephone = telephone });
            return restos;
        }

        public void AjouterUnRestaurant(string nom, string telephone)
        {
            Restos.Add(new Resto { Id = Restos.Count, Nom = nom, Telephone = telephone });
        }

        public void ModifierRestaurant(int id, string nom, string telephone)
        {
            Resto restoTrouve = Restos.FirstOrDefault(resto => resto.Id == id);
            if (restoTrouve != null)
            {
                restoTrouve.Nom = nom;
                restoTrouve.Telephone = telephone;
            }
        }

        public List<Resto> ChangerRestaurant(List<Resto> restos, int id, string nom, string telephone)
        {
            Resto restoTrouve = restos.FirstOrDefault(resto => resto.Id == id);
            if (restoTrouve != null)
            {
                restoTrouve.Nom = nom;
                restoTrouve.Telephone = telephone;
            }
            return restos;
        }

        public Resto GetRestoById(int id)
        {
            Resto restoTrouve = Restos.FirstOrDefault(resto => resto.Id == id);
            return restoTrouve;
        }
    }
}